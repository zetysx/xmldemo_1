/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author WIN7
 */
public class StudentDTO {
    private String id;
    private String firstname;
    private String lastname;
    private String sex;
    private String password;
    private String status;
    private String classStudy;
    private String address;

    public StudentDTO(String id, String firstname, String lastname, String sex, String status, String address, String classStudy) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.sex = sex;
        this.status = status;
        this.address = address;
        this.classStudy = classStudy;
    }

    
    
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClassStudy() {
        return classStudy;
    }

    public void setClassStudy(String classStudy) {
        this.classStudy = classStudy;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    

    @Override
    public String toString() {
        return "StudentDTO{" + "id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", sex=" + sex + ", password=" + password + ", status=" + status + ", classStudy=" + classStudy + '}';
    }
    
    
}
