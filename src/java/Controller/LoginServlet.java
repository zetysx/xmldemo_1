/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Utils.XMLUtilities;
import Utils.XMLUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author WIN7
 */
public class LoginServlet extends HttpServlet {

    private final String INVALID = "invalid.jsp";
    private final String HOME = "home.jsp";
    private final String XML_FILE = "/WEB-INF/sourceXML.xml";
    private boolean bFound;
    private String fullname;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = INVALID;

        try {
            String username = request.getParameter("txtUsername");
            String password = request.getParameter("txtPassword");
//            String realPath = getServletContext().getRealPath("/");
//            String xmlPath = realPath + XML_FILE;
//            Document doc = XMLUtilities.parseFileToDOM(xmlPath);
            ServletContext context = this.getServletContext();
            Document doc = (Document) context.getAttribute("DOCUMENT");
            
            if (doc != null) {
                this.bFound = false;
                this.fullname = "";
                this.checkLogin(username, password, doc);
                if (this.bFound) {
                    url = HOME;
                    HttpSession session = request.getSession();
                    session.setAttribute("ID", username);
                    session.setAttribute("FULLNAME", fullname);
                }
            }
            request.getRequestDispatcher(url).forward(request, response);
        }  catch (Exception ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();

        }
    }

    private void checkLogin(String username, String password, Node node) {
        if (node == null || bFound) {
            return;
        }
        if (node.getNodeName().equals("student")) {
            NamedNodeMap attrs = node.getAttributes();
            String id = attrs.getNamedItem("id").getNodeValue();
            if (id.equals(username)) {
                NodeList chhildrenOfStudent = node.getChildNodes();
                for (int i = 0; i < chhildrenOfStudent.getLength(); i++) {
                    Node child = chhildrenOfStudent.item(i);
                    String nodeName = child.getNodeName();
                    if (nodeName.equals("lastname")) {
                        this.fullname = child.getTextContent().trim();
                    } else if (nodeName.equals("firstname")) {
                        this.fullname = this.fullname + " " + child.getTextContent().trim();
                    } else if (nodeName.equals("password")) {
                        String pass = child.getTextContent().trim();

                        if (!pass.equals(password)) {
                            break;
                        }
                    } else if (nodeName.equals("status")) {
                        String status = child.getTextContent().trim();
                        if (!status.equals("dropout")) {
                            this.bFound = true;
                            return;
                        }
                    }

                }
            }
        }

        NodeList children = node.getChildNodes();
        int i = 0;
        while (i < children.getLength()) {
            this.checkLogin(username, password, children.item(i++));
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
