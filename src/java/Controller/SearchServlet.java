/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DTO.StudentDTO;
import Utils.XMLUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author WIN7
 */
public class SearchServlet extends HttpServlet {

    private final String SEARCH = "home.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String searchValue = request.getParameter("searchValue");
        String url = SEARCH;
        try {
            if (searchValue.trim().length() > 0) {
                ServletContext context = this.getServletContext();
                Document doc = (Document) context.getAttribute("DOCUMENT");
                if (doc != null) {
                    String exp = "//student[contains(address,'"
                            + searchValue
                            + "')]";
                    XPath xpath = XMLUtilities.getxPath();
                    NodeList studentList = (NodeList) xpath.evaluate(exp, doc, XPathConstants.NODESET);
                    if (studentList != null) {
                        List<StudentDTO> result = null;
                        for (int i = 0; i < studentList.getLength(); i++) {
//                            String attr = "@id";
//                            String id = getAttr(i, attr);
//                            attr = "firstname";
//                            String firstname = getAttr(i, attr);
//                            attr = "lastname";
//                            String lastname = getAttr(i, attr);
//                            attr = "sex";
//                            String sex = getAttr(i, attr);
//                            attr = "status";
//                            String status = getAttr(i, attr);
                            Node student = studentList.item(i);
                            exp = "@id";
                            String id =  (String) xpath.evaluate(exp, student, XPathConstants.STRING);
                            exp = "firstname";
                            String firstname =  (String) xpath.evaluate(exp, student, XPathConstants.STRING);
                            exp = "lastname";
                            String lastname =  (String) xpath.evaluate(exp, student, XPathConstants.STRING);
                            exp = "sex";
                            String sex =  (String) xpath.evaluate(exp, student, XPathConstants.STRING);
                            exp = "status";
                            String status =  (String) xpath.evaluate(exp, student, XPathConstants.STRING);
                            exp = "address";
                            String address =  (String) xpath.evaluate(exp, student, XPathConstants.STRING);
                            exp = "@class";
                            String sClass =  (String) xpath.evaluate(exp, student, XPathConstants.STRING);
                            StudentDTO dto = new StudentDTO(id, firstname.trim(), lastname.trim(), sex.trim(), status.trim(), address.trim(),sClass);
                            if (result == null) {
                                result = new ArrayList<>();
                            }
                            result.add(dto);
                        }
                        request.setAttribute("result", result);
                        request.setAttribute("searchValue", searchValue);
                        if (result != null) { 
                            
                        }
                    }
                }
            }
        } catch (Exception e) {

        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

//    private String getAttr(int i, String attr) throws XPathExpressionException {
//        ServletContext context = this.getServletContext();
//        Document doc = (Document) context.getAttribute("DOCUMENT");
//        XPath xpath = XMLUtilities.getxPath();
//        String exp = "//student[" + i + "]/" + attr;
//        String attrVal = (String) xpath.evaluate(exp, doc, XPathConstants.STRING);
//        return attrVal;
//    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
