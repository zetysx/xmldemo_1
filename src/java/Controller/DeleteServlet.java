/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DTO.StudentDTO;
import Utils.XMLUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author WIN7
 */
public class DeleteServlet extends HttpServlet {
    private final String SEARCH = "home.jsp";
    private final String XML_FILE = "/WEB-INF/sourceXML.xml";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String id =  request.getParameter("id");
        String lastSearch =  request.getParameter("lastSearch");
        String url = SEARCH;
        try {
                ServletContext context = this.getServletContext();
                Document doc = (Document) context.getAttribute("DOCUMENT");
                if(doc!=null) {
                    String exp = "//student[@id= '"+id+"']";
                    XPath xpath = XMLUtilities.getxPath();
                    Node student = (Node) xpath.evaluate(exp,doc, XPathConstants.NODE);
                    if(student !=null) {
                        Node parent = student.getParentNode();
                        parent.removeChild(student);
                        String xmlPath = context.getRealPath("/") + XML_FILE;
                        XMLUtilities.convertDOMtoFile(doc, xmlPath);
                    }
                }
                String urlRewriting = "SearchServlet?searchValue=" + lastSearch;
                response.sendRedirect(urlRewriting);
        } catch (Exception e) {
            
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
