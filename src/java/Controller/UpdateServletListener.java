/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Utils.XMLUtilities;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Web application lifecycle listener.
 *
 * @author WIN7
 */
public class UpdateServletListener implements ServletContextListener {

    private final String XML_FILE = "/WEB-INF/sourceXML.xml";
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Updating...............");
        ServletContext context = sce.getServletContext();
        String realPath = sce.getServletContext().getRealPath("/");
        String xmlPath = realPath + XML_FILE;
        try {
            Document doc = XMLUtilities.parseFileToDOM(xmlPath);
            context.setAttribute("DOCUMENT", doc);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(UpdateServletListener.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(UpdateServletListener.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UpdateServletListener.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        
    }
}
