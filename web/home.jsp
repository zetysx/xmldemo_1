<%-- 
    Document   : home
    Created on : Oct 1, 2018, 11:05:00 AM
    Author     : WIN7
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
    </head>
    <body>

        <h3>Welcome, ${sessionScope.FULLNAME} </h3>
        <h1>Search page</h1>
        <form action="SearchServlet">
            Search: <input type="text" name="searchValue" value="${searchValue}"/>
            <input type="submit" value="Search"/>
        </form>
        <c:if test="${not empty requestScope.result}">
            <c:set var="result" value="${requestScope.result}"/>
            <table border="1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ID </th>
                        <th>Full name</th>
                        <th>Sex</th>
                        <th>Class</th>
                        <th>Status</th>
                        <th>Address</th>
                        <th>Delete</th>
                        <th>Update</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="dto" items="${result}" varStatus="counter">
                    <form action="UpdateServlet">
                        <tr>
                            <td>${counter.count}</td>
                            <td> ${dto.id} <input type="hidden" name="hiddenID" value="${dto.id}"/> <input type="hidden" name="lastSearch" value="${searchValue}"/> </td>
                            <td> ${dto.firstname} ${dto.lastname}</td>
                            <td> ${dto.sex} </td>
                            <td> <input type="text" name="txtClass" value="${dto.classStudy}" /> </td>
                            <td> <input type="text" name="txtStatus" value="${dto.status}" />  </td>
                            <td> ${dto.address} </td>
                            <td> <a href="DeleteServlet?id=${dto.id}&lastSearch=${searchValue}">Delete</a> </td>
                            <td> <input type="submit" value="Update" /> </td>
                        </tr>
                    </form>
                </c:forEach>
            </tbody>
        </table>
    </c:if>
    <c:if test="${empty requestScope.result && not empty searchValue}">
        <h1>No result found!</h1>
    </c:if>
</body>
</html>
